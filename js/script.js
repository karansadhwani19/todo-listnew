//id of all the elements
var addTaskButton=document.getElementById("button-add");
var addInfoPanel=document.getElementById("add-info");
var addinfoTextArea=document.getElementById("add-infoTextArea");
var prioritySelect=document.getElementById("selectPriority");
var addDescTextArea=document.getElementById("add-descTextArea");
var panelSubmitButton=document.getElementById("submitButton");
var contentTasks=document.getElementById("tasks1");
var expanderTask=document.getElementsByClassName("down");
var deleteTask=document.getElementsByClassName("info");
var countId=localStorage.length;
addInfoPanel.style.display="none";

//displaying panel for adding data
addTaskButton.addEventListener('click',function(){
    addInfoPanel.style.display="block";        
});

//function to add task in web-localStorage
function add(){
    if(addinfoTextArea.value.length===0 || addDescTextArea.value.length===0){
        alert("Subject or Description is Empty!!");
    }
    else{
        console.log("hey");
        countId++;
        console.log(prioritySelect.value);
        var random={
            subject:addinfoTextArea.value.trim(),
            priority:prioritySelect.value,
            description:addDescTextArea.value.trim()
        }
        window.localStorage.setItem(countId,JSON.stringify(random));        
    }
    addInfoPanel.style.display="none";
    location.reload();
}
panelSubmitButton.addEventListener("click",add);//calling add on panel to submit data

document.onload=fetch();//calling fetch on-load()
function fetch(){
    if(window.localStorage.length===0){
        alert("No Tasks Yet!!!!!!!!!");
    }
    else{
        //getting data from web-localStorage
        for(i=1;i<=localStorage.length;i++){
            let fetchedData=JSON.parse(localStorage.getItem(i));
            var sub=fetchedData.subject;
            var priority=fetchedData.priority;
            var desc=fetchedData.description;
            //creating list and its associated icons
                var liElement=document.createElement("li");
                var iconExpand=document.createElement("i");
                var iconBin=document.createElement("i");
                var iconPriority=document.createElement("i");
                
            //appending li to tasks panel 
                contentTasks.appendChild(liElement);
                liElement.innerHTML=sub;

            //setting attributes    
                liElement.setAttribute("id","task"+i);
                iconExpand.setAttribute("class","down fas fa-chevron-down");
                iconBin.setAttribute("class","info fas fa-trash");
                iconPriority.setAttribute("class","circle fas fa-circle");
                iconBin.setAttribute("id",countId);

            //appendChild to list item created
                document.getElementById("task"+i).appendChild(iconPriority);
                document.getElementById("task"+i).appendChild(iconBin);
                document.getElementById("task"+i).appendChild(iconExpand);
        
            //paragraph for adding description
                var para=document.createElement("p");
                para.setAttribute("class","para");
                para.innerHTML="Description: "+desc;
                liElement.appendChild(para);
            //setting priority
                var prio=document.createElement("p");
                prio.setAttribute("class","para");

            //prioritywise setting the color
                if(priority=="High"){
                    prio.setAttribute("class","High");
                    iconPriority.classList.add("High");
                }
                else if(priority=="Medium"){
                     prio.setAttribute("class","Medium");
                     iconPriority.classList.add("Medium");
                }
                else if(priority=="Low"){
                    prio.setAttribute("class","Low");
                    iconPriority.classList.add("Low");
                }
            //displaying priroity when extends icon is clicked
            prio.innerHTML="Priority: "+priority;
            liElement.appendChild(prio);
        }  
    }
//looping to add event-listeners
    for(var i=0;i<=expanderTask.length;i++){
        if(countId>=1){
            expanderTask[i].addEventListener('click',expandInfo);
            deleteTask[i].addEventListener('click',deleteInfo);
        }
    }
}

//function for expanding the item to view details like
function expandInfo(){
            k=this.parentElement.getAttribute("id");
            this.classList.add("fas");
            if(document.getElementById(k).classList.contains("expander")){
                document.getElementById(k).classList.remove("expander");       
                this.setAttribute("class","");
                this.setAttribute("class","down fas fa-chevron-down");
            }
            else{
                document.getElementById(k).setAttribute("class","expander");  
                this.setAttribute("class","");
                this.setAttribute("class","down fas fa-times");
            }
    }

//function for deleting an item
function deleteInfo(){
    s=this.parentElement.getAttribute("id");
    document.getElementById(s).remove();
    localStorage.removeItem(this.id);
    location.reload();
}











